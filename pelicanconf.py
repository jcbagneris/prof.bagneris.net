#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Site
SITENAME = "local.bagneris.net"
SITESUBTITLE = "they said I should write it somewhere"
SITEURL = ''
TIMEZONE = 'Asia/Ho_Chi_Minh'

# Author(s)
AUTHOR = 'JcB'
AUTHORS_SAVE_AS = ''
AUTHORS_BIO = {
        "jcb": {
            "fullname": "Jean-Charles Bagneris",
            "image": "/img/jcb.jpg",
            "location": "Hanoi, Vietnam",
#            "website": "https://prof.bagneris.net",
#            "twitter": "jcbagneris",
#            "bio": "I write stuff."
            }
        }

# Themes
THEME = "attilight-dev"

# attilight theme
HEADER_BG_IMG = '/img/cover_img_dark.png'
#HEADER_BG_COLOR = '#002b36'
FOOTER_BG_COLOR = '#002b36'
SHOW_FULL_ARTICLE = False
LANG_NAME = {
        'en': 'English',
        'fr': 'French',
}

# Contents
PATH = 'content'
IGNORE_FILES = ['__pycache__',]
STATIC_PATHS = ['img','stuff']
#USE_FOLDER_AS_CATEGORY = True
PATH_METADATA = '(?P<category>.*?)/'
DEFAULT_LANG = 'en'
DEFAULT_DATE = 'fs'
DEFAULT_METADATA = {
        'status': 'draft',
        }
SLUGIFY_SOURCE = 'basename'


# Plugins
PLUGIN_PATHS = ["/home/jcb/work/projets/writing/tools/pelican/plugins",]
PLUGINS = [
            "render_math",
        ]

# Rendering
TYPOGRIFY = True
DEFAULT_PAGINATION = 5

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = True

#DOCUTILS_SETTINGS = {
#        'math_output': 'MathJax https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js',
#        }

PYGMENTS_RST_OPTIONS = {
        'linenos': 'none',
        'linenospecial': 3,
        }

MENUITEMS = (
        ('home', '/'),
        ('archives', '/archives.html'),
        ('tags', '/tags.html'),
        ('about', '/pages/about-en.html'),
#        ('categories', '/categories.html'),
        )

ARTICLE_SAVE_AS = 'posts/{date:%Y}/{slug}-{lang}.html'
ARTICLE_URL = ARTICLE_SAVE_AS
ARTICLE_LANG_SAVE_AS = ARTICLE_SAVE_AS
ARTICLE_LANG_URL = ARTICLE_SAVE_AS

PAGE_SAVE_AS = 'pages/{slug}-{lang}.html'
PAGE_URL = PAGE_SAVE_AS
PAGE_LANG_SAVE_AS = PAGE_SAVE_AS
PAGE_LANG_URL = PAGE_SAVE_AS

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'

LINKS = (
#        ('github', 'https://github.com/jcbagneris/'),
        ('gitlab', 'https://gitlab.com/jcbagneris'),
         )
SOCIAL = (
        ('linkedin', 'https://linkedin.com/in/jcbagneris'),
        ('twitter', 'https://twitter.com/jcbagneris'),
        )

# Development
LOAD_CONTENT_CACHE = False
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

RELATIVE_URLS = True
