about this website
==================

:slug: about
:lang: en
:authors: JcB
:status: published
:date: 2017-04-28 15:41
:modified: 2017-07-25 12:11

My name is Jean-Charles Bagneris, I work mainly as a corporate finance professor
and this is my professional website and blog, hence the ``prof.bagneris.net``
url. I must confess I am a bit of a computer addict. I especially like
automating boring stuff and making scripts to help me in my teaching activities.

This is the place where I mainly put my *other contributions*, as well as
ramblings, notes and the like. Professors are expected to publish research,
textbooks, case studies, and "other contributions", that is,  whatever does
not fit in the other categories.

Opinions expressed here are solely mine, of course, and are not necessary
endorsed or shared by my supports.

You can contact me by email at jcbagneris on Google mail.

|Creative Commons License| Except when otherwise specified, all contents
on this website are licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License
http://creativecommons.org/licenses/by-nc-sa/4.0/. The terms of this
license allow you to remix, tweak, and build upon this work
non-commercially, as long as you credit me and license your new
creations under the identical terms.

Most of the work reported here was done with some support from
`Montpellier Business School <http://www.montpellier-bs.com/>`__ and the
CFF company.

.. |Creative Commons License| image:: /img/cc-by-nc-sa.png
   :class: inlineimg
