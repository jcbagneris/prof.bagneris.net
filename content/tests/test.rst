Test
====

:status: draft
:tags: test
:authors: JcB, anhj
:category: workshop

.. NOTE::
    Note that Slug and Date are not strictly necessary as they are inferred
    automatically.

Authors and Lang both have defaults, defined in settings.

An unordered [#]_ list:

-  one
-  two
-  three

Another one, ordered:

#. one
#. two
#. three

A table:

.. raw:: html

    <table>
    <thead>
    <tr>
    <th>Lastname</th>
    <th>Firstname</th>
    <th class="right">Balance</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Doe</td>
    <td>John</td>
    <td class="right">12.54</td>
    </tr>
    <tr>
    <td>Xy</td>
    <td>Alice</td>
    <td class="right">36.40</td>
    </tr>
    </tbody>
    <tfoot></tfoot>
    </table>

Some code
---------

.. WARNING::
    Don't try this at home, kids!

.. code-block:: python
   :linenos: inline

    #!/usr/bin/env python3.6
    # -*- encoding: utf-8 -*-
    """
    flatten.py
    =========|=========|=========|=========|=========|=========|=========|=========|
    Flatten tex files by following all the \input{foobar}
    Returns the flattened file on stdout
    Usage: flatten.py < first.tex > flattened.tex
    """

    import sys
    import re

    target = re.compile(r'^\\input{(.*)}')

    def flatten(lf=[]):
        if lf:
            fsource = open(lf[-1],'rt')
        else:
            fsource = sys.stdin
        for line in fsource:
            mtarget = re.match(target, line)
            if mtarget:
                ftarget = mtarget.group(1)
                if not ftarget.endswith('.tex'):
                    ftarget = f"{ftarget}.tex"
                    if ftarget in lf:
                        raise RecursionError(
                          f"Circular error in {line.split()}: {ftarget} already parsed in {lf}.\n"
                            )
                    else:
                        yield from flatten(lf + [ftarget])
            else:
                yield line
        if lf:
            fsource.close()



    for line in flatten():
        sys.stdout.write(line)


Blah

.. rubric:: Footnotes

.. [#] And here is a footnote

