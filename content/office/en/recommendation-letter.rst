So you need a recommendation letter
===================================

:slug: recommendation-letter
:lang: en
:authors: JcB
:status: published
:date: 2017-05-09 18:49
:modified: 2017-05-11 16:17
:tags: advice, recommendation, curriculum
:summary: Dear student, let me explain why I did not write a recommendation
          letter for you -- and how to increase the probability that I agree to
          write it next time you ask.

Dear student,

This morning I received a polite email from you, asking for a
recommendation letter. Sure, your email was polite (sometimes even
friendly), but it was nonetheless short, not very informative, and
rather unexpected. I replied with another email, short and polite as
yours, but not very personal I am afraid: it is a standard email
explaining in a few words why I will not write a recommendation for you.
I prepared it some time ago. The reason is very simple: *I have
absolutely no idea who you are*. Your name does not remind me of
anything, and even if it did, I would not be able to remind your face,
of when I met you, or which course you followed (I could not even tell
if you truly are or were a student of mine, or at least a student of the
school I work for). Still, the short and standard email I sent might
seem a bit rude, so I decided to elaborate a longer answer. Read ahead
if you are interested.

So you apply to another program, which is probably a good idea, and of
course you are not alone, especially if this program has a good
reputation. In addition to various selection techniques (academic
results, written report, interview), most business school or
universities also ask for recommendations of some sort. It goes from the
traditional recommendation letter to online or paper forms containing
(too) many questions about you, the applicant. These questions more or
less relate to one major one: will you be able to follow the said
program? This should make one point obvious, the person recommending you
should know you quite well, not personally, but at least in a learning
context. This is why it is often advised you ask a former professor to
write the recommendation for you.

Asking a former professor does not mean asking a random professor, or
one you liked, or one who looked to be "a nice guy" who surely would not
refuse as it is so important for you. If you want to get an efficient
recommendation, you should decide carefully who to ask for it (and of
course, how to ask for it, more on this later).

Who to ask for a recommendation?
--------------------------------

The obvious answer is to try to ask someone who is able to answer the
main question above, someone who *can* tell about you in a learning
context. This might be a professor, a supervisor, the director of a
program, an advisor... The important point is that this person should
know you. When seeing your email, she should tell herself "Ah, an email
from Linda, I wonder if she finally applied to Somewhere University",
not "What is this again? Spam or what?".

The problem is that with a professor, mutual relationship is typically
asymmetric: students know me far better than I know them. My students
know me well because they spent quite some time listening to me in
class, they can even probably tell some of my weaknesses, or the way I
say "ok?" every five minutes, or some things I don't want to know. They
remember this time when they came to my office to ask a question and I
spent 15 minutes explaining at the white board.

But I don't.

I don't remember this particular student, not even this particular class
most of the time. I don't remember you came to my office with a
question, as this happens every day. When you are in the classroom, you
see one person, the professor. You remember you visited me because it
happened only once to you. When I am in the classroom, I see students,
usually more than 40 of them. Students visit my office everyday with
questions, and yes, I spend time answering them, it is (part of) my job.

So, to improve you chances of success, think about someone who, for some
reason, might know (or at least remember) you. Or, if you insist on me
writing this letter, try to find some reasons I might remember you.
Maybe I was your supervisor for a thesis? Maybe when you came to my
office you noticed a particular book on my desk, and we spent an
additional 20 minutes chatting about literature, science-fiction or
computer programming? Or you asked me for an advice before applying to
this very program? All of this increases greatly the probability that I
remember you, especially if you kindly remind me of it -- which leads us
to the next part, how to ask.

How to ask for a recommendation?
--------------------------------

When you write the email asking for the recommendation, never assume
that I will remember you at first sight. Introduce yourself, tell who
you are first, then how we know each other (this is the moment to remind
me of the time we talked about computer programming), and finally what
you need and why (and this is the moment to remind me I, myself, advised
you to apply for this or that program). This is necessary to make me
*consider* writing the letter for you.

Then, in case I decide to help you, help me first! Remember that in the
first lines of this post up there I wrote that your email was
uninformative? Even if I remember you and I am willing to help, there
are many things I don't know about you, or I have to search for
information, or worse, ask for it. The more I have to do, the less I
want to do it, of course. I am a human, too. So, write a short paragraph
explaining where you apply, why, what are your motivations, how it
follows logically from what you did before. And then, join the
information I might need, at the very least a resume, including a photo
(the photo alone increases greatly the odds I finally remember you).
Some people think it is better to wait for a positive answer before
sending additional information. I think it is less efficient. Of course,
no need to include 10 Go of all your personal records, back to
kindergarden. Select relevant information.

Do not forget to include the details about how to provide the letter, or
reply to the form. Should it be handwritten? Should it be printed on the
school's official paper? Where and when should it be sent?

Finally, all this, choosing a professor who might know you, and
providing organized and necessary information has the same objective:
reduce the information asymmetry. Actually, this is the objective of the
recommendation letter as well: the program you are applying for does not
know you, so it asks for an information transfer from someone who
hopefully does. Chose this person carefully, and make sure you help her
help you.

I wish you all the best in the pursuit of your project.

Sincerely
