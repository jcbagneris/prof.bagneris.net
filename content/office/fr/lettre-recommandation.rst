Et donc, vous avez besoin d'une lettre de recommandation
========================================================

:slug: recommendation-letter
:lang: fr
:date: 2017-05-13 11:20
:status: published
:tags: advice, recommendation, curriculum

Cher étudiant(e),

Ce matin j'ai reçu un email poli de votre part, me demandant une lettre
de recommandation. Bien sûr, votre email était bien poli (parfois même
amical), mais il était aussi assez bref, pas très informatif, et plutôt
inattendu. J'ai répondu par un autre mail, court et poli comme le vôtre,
mais pas très personnel, j'en ai peur : c'était un email standard
expliquant en quelques mots pourquoi je n'écrirai pas de lettre de
recommandation pour vous. Je l'ai préparé il y a quelques temps. La
raison de mon refus est très simple : *je n'ai pas la moindre idée de
qui vous êtes*. Votre nom ne me dit rien, et même s'il me disait
quelque chose, je ne me souviendrais pas de votre visage, ou de quand
nous nous sommes rencontrés, ou lequel de mes cours vous avez suivi (en
fait je ne pourrais même pas affirmer que vous êtes ou étiez vraiment un
de mes étudiants, ou même un étudiant de l'école dans laquelle
j'enseigne). Quoiqu'il en soit, la réponse courte et standardisée que je
vous ai faite peut sembler un peu brutale, j'ai donc décidé de rédiger
quelquechose d'un peu plus détaillé. Lisez la suite si cela vous
intéresse.

Donc vous postulez dans un autre programme, ce qui est probablement une
bonne idée, et bien sûr, vous n'êtes pas le seul, particulièrement si ce
programme a une bonne réputation. En plus de différentes techniques de
sélection (résultats universitaires, entretien ou même rédaction d'un
mémoire), la plupart des écoles et universités demandent aussi des
recommandations de différentes formes. Cela va de la traditionnelle
lettre de recommandation au questionnaire en ligne ou sur papier, posant
de (trop) nombreuses questions sur vous, le candidat. Toutes ces
questions se rapportent de plus ou moins près à la question centrale :
serez vous capable de réussir le programme en question ? Tout ceci
montre à l'évidence que la personne qui vous recommande doit très bien
vous connaître, si ce n'est personnellement, au moins dans un contexte de
formation. C'est la raison pour laquelle il est généralement conseillé
de solliciter un ex-professeur pour faire cette recommandation.

Solliciter un ex-professeur ne veut pas dire solliciter un professeur au
hasard, ou un que vous avez apprécié, ou un qui avait l'air sympathique
et donc "sera surement d'accord", c'est si important pour vous. Si vous
voulez être recommandé efficacement, vous devez soigneusement décider de
qui vous allez solliciter (et bien sûr, comment vous allez le faire,
voir plus loin).

A qui demander une recommandation ?
-----------------------------------

La réponse évidente est d'essayer de trouver quelqu'un qui est capable
de répondre à la question principale citée ci-dessus, quelqu'un qui
*peut* parler de vous dans un contexte de formation. Cela peut être un
professeur, un directeur de mémoire, un responsable de programme, un
conseiller d'apprentissage... Ce qui est important, c'est que cette
personne vous connaisse. En voyant votre email, cette personne devra se
dire "Ah, un email de Valérie, je me demande si finalement elle a
postulé à l'université de Somewhere", et non "Qu'est-ce que c'est encore
? Du spam ?".

Le problème, c'est qu'avec un professeur, votre mutuelle relation est
généralement assymétrique : mes étudiants me connaissent bien mieux que
je ne les connais. Ils me connaissent parce qu'ils ont passé pas mal de
temps à m'écouter en classe, au point qu'ils peuvent probablement citer
certains de mes points faibles ou tics de langage, ou d'autres choses
que je ne veux même pas savoir. Ils se souviennent de cette fois où ils
sont venus dans mon bureau pour me poser une question et où j'ai passé
quinze minutes à leur expliquer comment trouver la réponse sur le tableau
blanc.

Mais pas moi.

Je ne me souviens pas de cet étudiant là, ni même de cette promotion là
dans la plupart des cas. Je ne me souviens pas que vous soyiez venu à
mon bureau avec une question, parce que cela arrive tous les jours.
Quand vous êtes en salle de cours, vous voyez une personne en face de
vous, le professeur. Vous vous souvenez d'être venu me voir parce que
vous ne l'avez fait qu'une seule fois. Quand je suis dans la salle de
cours, je vois des étudiants, souvent plus d'une quarantaine. Des
étudiants passent me voir à mon bureau tous les jours avec des
questions, et oui, je prends le temps de leur répondre. C'est (une
partie de) mon travail.

Donc, pour augmenter vos chances de succès, essayez de trouver quelqu'un
qui, pour une raison ou pour une autre, vous connait (ou a au moins des
chances de se souvenir de vous). Ou, si vous voulez vraiment que ce soit
moi qui écrive cette lettre, essayez de trouver une raison pour laquelle
je pourrais me souvenir de vous. Peut-être que vous avez écrit un
rapport, un mémoire, fait un exposé sous ma direction ? Peut-être que ce
jour où vous êtes venu à mon bureau vous avez remarqué un livre qui
dépassait de mon sac, et nous avons passé vingt minutes de plus à parler
littérature, science-fiction, ou programmation? Ou vous m'avez demandé
mon avis avant de postuler à ce programme précis ? Toutes ces petites
choses augmentent largement la probabilité que je me souvienne de vous,
tout particulièrement si vous me le rappelez gentiment -- ce qui nous
amène à la section suivante, comment demander une recommandation.

Comment demander une recommandation ?
-------------------------------------

Lorsque vous écrivez votre email pour solliciter une recommandation, ne
supposez jamais que je vais me souvenir de vous au premier coup d'oeil.
Présentez-vous, dites d'abord qui vous êtes, puis comment nous nous
connaissons (c'est le moment de me rappeller cette discussion sur la
science-fiction), et finalement ce dont vous avez besoin et pourquoi (et
c'est le bon moment pour me rappeler que je vous ai moi-même conseillé
de postuler à ce programme). Tout ceci est nécessaire pour que
*j'envisage* de vous faire une recommandation.

Ensuite, s'il se trouvait que je décide de vous aider, aidez-moi d'abord
! Vous souvenez-vous que dans les premières lignes de ce post, un peu
plus haut, j'ai écrit que votre email n'était pas trop informatif ? Même
si je me souviens de vous et que je décide de vous aider, il y a
beaucoup de choses que j'ignore à votre sujet, ou bien je vais avoir à
chercher des informations, ou pire, demander à quelqu'un de chercher. Le
plus de travail cela me demandera, le moins j'aurais envie de le faire,
évidemment. Je suis humain, moi aussi. Donc, écrivez un court paragraphe
expliquant où vous postulez, pourquoi, quelles sont vos motivations, en
quoi cela complète logiquement votre cursus. Et bien sûr, joignez les
informations dont je pourrais avoir besoin, au minimum un CV, avec une
photo (rien que la photo augmente de beaucoup les chances que finalement
je me souvienne de vous). Certaines personnes pensent qu'il vaut mieux
attendre une réponse positive avant d'envoyer des informations
supplémentaires, je ne suis pas de cet avis. Bien sûr, ce n'est pas la
peine d'attacher au mail 10 Go d'informations personnelles en remontant
à la maternelle. Choisissez ce qui est pertinent.

N'oubliez pas d'inclure les détails sur la façon de fournir la lettre,
ou de répondre au formulaire. La lettre doit-elle être manuscrite ?
Faut-il au contraire l'imprimer sur le papier à entête de l'école ? Où
et quand l'envoyer ?

Finalement, tout ceci, choisir un professeur qui vous connaît, lui
fournir l'information nécessaire de façon organisée, poursuit le même
objectif : réduire l'assymétrie d'information. En fait, c'est aussi
l'objectif de la lettre de recommandation : le programme auquel vous
postulez ne vous connait pas, c'est la raison pour laquelle il demande
un transfert d'information à quelqu'un qui, lui, vous connait.
Choisissez cette personne soigneusement, et faites le maximum pour
l'aider à vous aider.

Je vous souhaite beaucoup de réussite dans la poursuite de votre projet.

Sincèrement
