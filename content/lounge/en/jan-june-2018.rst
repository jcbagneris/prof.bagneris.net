January-June 2018 in review
===========================

:date: 2018-06-29 10:54
:tags: review, make, writing, haskell, pelican, hakyll
:status: published
:summary: A quick review of the first months of 2018, what I did and what I did
          not (yet) do, given my current priorities.


It's been a while I did not post anything -- the last time was when I was
`playing`_ with `GNU make`_.

.. _playing: {filename}/workshop/en/makefiles.rst
.. _GNU make: {filename}/workshop/en/makefiles-2.rst

As the first half of 2018 is coming to an end, I started to think about what I
did into these six months, and how it fitted with my priorities for the year --
I do not really have what one would call "new year resolutions", but I try to
give myself priorities, and stick to those.

Priorities
----------

Broadly speaking, my top priority is to always learn something new (and
hopefully useful), every day if possible. But as I tend to be attracted by many
topics, having a rough idea of what to *mainly* learn is necessary to maintain
some kind of focus. Besides learning for myself, which is related to improving,
I also try to find a development priority in my job. This is important as well,
as, when I am not in a teaching period, I am mostly completely independent on a
daily basis (I do have an agreement on a 5-years plan with my main employer
though).

So my current priorities are:

- To carry on and develop writing material on the main topic I teach (corporate
  finance). I want to be able to provide free and quality material to my
  students, without the need for them to buy any textbook.

- To rationalize the way I am writing, to improve the set of helper scripts I
  wrote to help, and to learn better the main tools I use for all that (pandoc_,
  vim_, make_, python_).

- To learn a new programming language, haskell_.

- To learn about NLP or "Natural Language Processing" as I think there are
  numerous applications of NLP in the teaching/education fields.

.. _pandoc: http://pandoc.org
.. _vim: https://www.vim.org
.. _make: 
    https://www.gnu.org/software/make/manual/html_node/index.html
.. _python: https://www.python.org
.. _haskell: https://www.haskell.org

Note that another high ranked priority for me is to explore topics I neglected
when I was younger, and especially philosophy. The idea is to get myself a
clearer "plan for living" or "philosophy of life". I will not elaborate on this
here today, as it is more a path than an objective (the travel is more important
than the destination). It is also rather personal of course.

Writing
-------

I started to write some kind of handouts or chapters for my students last year
(around January 2017). The idea was to be able to provide written documents of
15 to 30 pages for all the topics I am teaching in my main course, corporate
finance. Even if the objective is not (yet?) to write a book, I like the idea of
viewing those as some kind of chapters: there is a logical progression in the
topics, and later chapters build on earlier ones.

I am aware that there are plenty of very good finance textbooks around, and I
certainly do not want to compete with their authors. Still, most of these books
are pricey, some very good ones are no longer available, and I confess that
after more than 25 years of experience, I felt I might bring a little something
to the edifice.

Each document (or handout, or chapter) was written following these rules:

- write in English first, translate to French later,

- provide learning objectives in the beginning and a summary in the end,

- give a lot of practical, as close as possible to "real life" examples,

- provide exercises, and answers to those in the end of the document,

- use versioning for all documents (`calendar based versioning`_),

- make them `freely available`_ and `open source`_: most of the documents have a
  Creative Commons license.

.. _calendar based versioning: http://calver.org
.. _freely available: https://files.bagneris.net/
.. _open source: https://gitlab.com/jcbagneris/finance-sources

In the course of this semester I reorganized a bit the full thing and got this
structure:

- helpers (bibliography, glossary, cheatsheets, ...)

- tools (how to use a spreadsheet, how to read financial statements, ...)

- valuation (time value of money, ...)

- financing (debt, equity)

- risk management

- portfolio management

And finally I wrote four new "chapters" and updated the global bibliography:

- Finance bibliography v2018.02.1

- Stocks valuation v2018.03.2

- Fundamentals of risk and return in finance v2018.04.1

- The weighted average cost of capital v2018.05.1

- The investment decision process v2018.06.1

Yes, that is roughly one document per month. I am a slow writer, I
procrastinate, I am a perfectionist, and I have time.

Finally, on the writing side, I also wrote three posts on this blog, `two
about`_ `GNU make`_ and this one.

.. _two about: {filename}/workshop/en/makefiles.rst

Tools
-----

Writing helpers
~~~~~~~~~~~~~~~

As you probably know already, `I write everything in pure text`_, using mainly
`pandoc markdown`_, but also reStructuredText_ and LaTeX_.

.. _I write everything in pure text:
    {filename}/workshop/en/workflow-1-writing-in-text.rst
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _LaTeX: http://www.latex-project.org/
.. _pandoc markdown: http://pandoc.org/MANUAL.html#pandoc-markdown

To ease the process of producing the handouts and chapters referred to in the
previous section, I wrote some filters, snippets and makefiles to go with
pandoc_.

After nearly one year, the whole repository started to be a big mess and needed
some kind of refactoring. I also wanted to have a single makefile, able to
generate the final pdf document from either `pandoc markdown`_ or LaTeX_ sources.

So I started with the re-organization and refactoring of the tools. The main
idea was to anticipate on the different kind of documents I would have to
produce. I ended with the following categories:

- a ``chapter`` is a written "course" material, again, the ones referred to in the
  previous section,

- ``slides`` might be produced with beamer_, with the associated ``handout`` for
  distribution to the students,

- a ``problem``, some ``exercises`` and ``answers`` for either one or the others, when
  available,

- and finally, a ``shortdoc`` is a 1-2 page document, for example a cheatsheet or
  a bibliography.

All these documents might be either public (and then available on
https://files.bagneris.net) or private: the slides and problem answers are not
usually freely available. In addition, as all the sources are managed with git_
and the repository is public_, sources might be encrypted if I want to keep them
secret. The makefile producing the pdf would decrypt the source on the fly if
the required private key is available (i.e. on my laptop, but not on yours).

.. _beamer: https://www.ctan.org/pkg/beamer?lang=en
.. _git: https://git-scm.com
.. _public: https://gitlab.com/jcbagneris/writing-helpers

Once the refactoring was done and things better organized, I finally could
finish the ``generic.mk`` makefile, which, as we have seen, can take clear or
encrypted sources either in `pandoc markdown`_ or in LaTeX_, and some metadata,
to:

- produce the final LaTeX_ source (including preamble, table of contents, last
  page footer etc.),
  
- make the pdf version for the source,

- install it locally (by hard-linking to a destination directory),

- upload it to https://files.bagneris.net, updating the index accordingly,

- rollback to the previous version if the upload did not go well.

Of course, as it is a makefile, it does only what is necessary: if you do not
change the sources and then make the pdf, it will not be recreated if it exists
already.

This makefile should be present in the directory of each source document, and
is designed to take care of the sources available in this directory.

I then wrote another makefile, ``main.mk``, which is intended to be at the root of
all the sources directory, and can be used to do the above actions (make the
pdf, install locally, upload or rollback) recursively for all the sources. It is
not fully tested yet, but usable, and the latest versions available where
uploaded with it.

Toolset
~~~~~~~

About the software I use, I tried also to rationalize a bit, and to improve my
workflow. The idea was to limit the number of tools, and to master those
better.

A first example of this is the way I was taking and maintaining notes about
everything. I have been using zim_ for some years, but was not fully happy with
it: I could not have a copy of my notes on my phone, on the laptop I had to
start zim_ to take any note instead of using my usual text editor (vim_), its
syntax, although not far from markdown, was specific, and it was a bit
unstable. In addition, I noticed that I was often only collecting bookmarks on
some topics, instead of taking real notes, and felt it was a bit stupid to keep
a set of bookmarks somewhere else than where they belong, the web browser.

So I recently switched to another system:

- I am back to the basics and keep all the bookmarks in my web browser
  (firefox_), and use tags for the organization,

- I started to use vimwiki_ to take notes and quick notes, using markdown as the
  markup and vim_ as the editor.

- I installed and set up termux_ on my phone: this means that I can have vim_ and
  vimwiki_ on the phone as well, and synchronize my notes between the laptop and
  the phone with syncthing_.

(The last steps, installing vimwiki_ on the phone and the synchronization of the
notes through syncthing_ are still to be done at the time I am writing this.)

.. _zim: http://zim-wiki.org
.. _vimwiki: https://github.com/vimwiki/vimwiki
.. _syncthing: https://syncthing.net
.. _termux: https://termux.com
.. _firefox: https://www.mozilla.org/en-US/firefox/new/

It was also the opportunity to have a look in my vim_ setup, the plugins I
really use or forgot, and generally increase my knowledge of, and confidence
with, this text editor, one of the piece of software I use the most. This of
course is a work in progress, some notable improvements are already visible with
the use of completion and omni-completion for example.

A second example is related to this blog: so far it is produced using the
Pelican_ static website generator, which uses reStructuredText_ as the markup
language for the sources. It is not so bad, as that means I use vim_ to edit the
posts and Pelican_ is written in python_, thus I can hack it a bit when
necessary (it happened once_ already).

.. _Pelican: http://docs.getpelican.com
.. _once:
    https://github.com/getpelican/pelican-plugins/issues/928#issuecomment-325092878

Anyway, would it not be better to use `pandoc markdown`_ again for these posts?
I use this markup nearly every day (for my writings and for taking notes), and I
use reStructuredText_ only once in a while to post here. Thus I am considering
the following combination: pandoc_ (again) for the markdown to html rendering,
and hakyll_ for the static website generator. It is only an idea so far, but, as
hakyll_ is written in haskell_, which I am currently learning, it makes sense.
And, by the way, pandoc_ is written in haskell_, too.

.. _hakyll: https://jaspervdj.be/hakyll/

Learning
--------

On a more personal note now, what do we have on the improving, development and
learning side?

Haskell
~~~~~~~

The first point in my priority list was to learn "a bit of haskell_". I actually
began to read `Learn You a Haskell for Great Good`_ online in spring 2017, but
quickly gave up, for many reasons: I lacked time as I started to write the
aforementioned finance material, I lacked motivation as I did not have any real
project in haskell, and a bunch of other wrong reasons.

So I put "learn haskell" on my priority list for this year, and it goes better.
First I found a lot of resources I did not know about, and especially
https://github.com/bitemyapp/learnhaskell which is *very* opinionated, but
interesting. I am currently finishing chapter 11 exercises of the `Haskell
Book`_, which is certainly not as good as claimed by its author, but still very
usable, especially because of the numerous exercises it proposes. The various
exercises I did so far from this book and other resources as well are available
on https://gitlab.com/jcbagneris/haskell-exercises.

.. _Learn You a Haskell for Great Good: http://learnyouahaskell.com/
.. _Haskell Book: http://haskellbook.com/

It might seem strange that a corporate finance professor, not particularly
quantitative finance oriented, would like to learn yet another programming
language, especially haskell_, which has a reputation of being the ivory tower of
category theorists. This leads to two different questions, why learn
programming in the first place, and why haskell_.

Why learn programming? If you are, like me, someone who is not primarily working
with his hands, not regularly *making* something, like say, a wooden table for
your living room, or your own house, or whatever, you know you miss something.
You miss the pleasure of looking at something you made, genuinely proud of what
you achieved with your bare hands and a few tools. I did not really produce
anything hand-made recently, and, for me, the closest experience to this feeling
of accomplishment because you made something is when I wrote a chapter (see
above), and even more, when I successfully wrote a script_ or a `full
programming project`_. Learning to program as a side activity is challenging,
interesting, rewarding and useful. Try it!

.. _script: {filename}/workshop/en/moodle-calculated-questions.rst
.. _full programming project: https://pythonhosted.org/fms/

So, why learn haskell_? I already played with the python_ programming language,
and still have a lot to discover about it. But when I started to use pandoc_
(which, again, is written in haskell_), and had a look at the sources, I read the
strangest code I had ever seen. This of course excited my curiosity, and I
investigated a bit more. Pure, lazy, functional programming, all these had to
become meaningful to me. This is how it all started. Now, it makes sense to me
to learn it as I like to be able to hack on the tools I use, as it makes me
learn a safe and interesting way to code, as it is simply challenging.

Natural Language Processing
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two years ago I followed and completed Andrew Ng's `Stanford Machine Learning
course on Coursera`_. I did it to get the basics of machine learning in order to
be at least able to sort out the noise and the bullshit from the real
information about it in my daily venturing in the news. As with
cryptocurrencies, the number of self appointed experts who actually know nothing
but a few buzz words is rising all the time, and I prefer to avoid them.

.. _Stanford Machine Learning course on Coursera:
    https://www.coursera.org/learn/machine-learning/home/welcome

In addition to the idea of learning the bare minimum about it, I was also
interested in finding out if some applications might be useful for myself, and
especially for my teaching. I think that the difficult field of natural language
processing might lead to really interesting applications in teaching: we
actually teach and challenge our students by using mainly natural language
(written and spoken) as a communication media. I thus put NLP in a corner of my
head, then on a projects list, and finally in this year priorities. Well, I must
admit that, beyond a few bookmarks and identifying good material to read and
study, I did not make any progress here yet. To be honest, it would be really
surprising to see any change until next year, as I will be teaching from
September to the end of the year, thus my free time will reduce like the ice
cube in my "cốc trà đá" (glass of iced tea) under the heat of July in Hanoi.

See you in December!





