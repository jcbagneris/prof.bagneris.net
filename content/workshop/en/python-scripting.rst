Scripting in python for fun and profit
======================================

:status: draft
:tags: python, scripting, bash

.. _clint: https://pypi.python.org/pypi/clint/
.. _entr: http://entrproject.org/
.. _click: http://click.pocoo.org/6/
.. _invoke: http://www.pyinvoke.org/index.html
.. _bashing the bash:
    https://medium.com/capital-one-developers/bashing-the-bash-replacing-shell-scripts-with-python-d8d201bc0989

