Writing in pure text: choose a markup language
==============================================

:date: 2017/09/29 10:33
:status: draft
:tags: writing, text, workflow, rest, markdown, latex, markup
:summary:


.. Note::
    This post is the second one in a series dedicated to my
    workflow: which are the tools and methods I use when I
    have to write anything.

        1. `Why I always write in pure text`_
        2. Writing in pure text: Choose a markup language
        3. Building process: `Let's play with make`_ 

.. _Why I always write in pure text: {filename}./workflow-1-writing-in-text.rst

.. _Let's play with make: {filename}./makefiles.rst


Markup languages?
-----------------

In a `previous post <{filename}./workflow-1-writing-in-text.rst>`_, I
explained why I use pure text files as much as possible whenever I have to
produce contents. In this one, I will discuss the various markup languages I
tried already, and the ones I finally chose to use. Spoiler: the perfect
one-size-fits-all markup language does not exist, and depending on the task at
hand, you might use a different one.

First things first, what is a markup language? It is a way to enrich the text
you are writing by inserting text marks in it. These marks might be related to
the layout ("the right margin should be 2.5cm"), the text structure ("start a
new section here"), the text rendering ("display this word in italic"),
semantics ("this part is the main content, this one is a margin note"), and more
(insert an image, make this word an index entry, put the table of contents here
etc.).

Note that, strictly speaking, html and even pdf are markup languages. The
various files composing an epub book are mostly xml, which is a markup language
as well. The difference is that html, pdf and xml are supposed to be read and
written by machines, not humans: it is possible but really cumbersome to write html or
read it directly, and impossible to read, and probably to write anything more
practical than a proof of concept in pdf. Thus, I will focus here on
human-readable and editable flavors of markup languages. The idea is that the
human-editable version might be transformed in the desired output automatically
-- that is, by a program.

The marked text is called the *source*, this is the one you work on with your
favorite editor, that you commit in a source management system to keep an
history of the changes and revisions. Once the source is ready (or whenever you
want to see what your works looks like), you produce (*compile*) the final
content with a specialized program, which interprets the marks in the text and
does the necessary work and calculations to output what you described. Note that
there might be more than one such program for a given markup language, depending
on what you want to produce (a page on a website, a pdf document, an epub book,
slides etc.). You can even convert from one markup to another one if you want
to.

Markup, output and format
-------------------------

In the last twenty years or so, I tried and used many such markup languages. Here
is a list, in no particular order, and probably not complete:

- `LaTeX`_
- `reStructuredText`_
- various flavours of `markdown`_
- `asciidoc`_
- `txt2tags`_

.. _LaTeX: http://www.latex-project.org/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _markdown: https://daringfireball.net/projects/markdown/
.. _asciidoc: https://asciidoc.org
.. _txt2tags: http://txt2tags.org/

...and I even directly wrote some html more often than I wanted. Note that there
are many more, but these are probably the most common ones.

Nowadays, I mostly use `pandoc markdown`_, `LaTeX`_ and `reStructuredText`_, depending
on what I want to produce as a final content. How and why did I finally chose
those?

.. _pandoc markdown: http://pandoc.org/MANUAL.html#pandoc-markdown

To answer these questions, I will first describe the various kind of contents I
usually produce, and the requirements I have for any of those.

The written documents I produce are mostly:

- blog posts, such as this one,
- slides for courses and lectures,
- handouts, short papers, exercises and problems or case studies for my
  teaching,
- textbooks once in a while,
- technical documentation for the various pieces of software I sometimes write
  or contribute to.

All of these are mainly intended to be delivered as electronic contents: again,
this might be pdf, epub files or html pages, but the idea is that the intended
audience will read my prose on a screen. Note that this does not prevent people
to print some of those, and the rendering should still be correct when printed.
But this is simply not the main purpose.

It is thus easy to classify the above list of documents along the file format of
the final product: either pdf, html or (marginally) epub. Again, nothing should
prevent a given source to be translated in *any* of those file formats, but
there is usually an obvious "standard" target for any of those.

==================== ===============
output                  format
==================== ===============
blog posts              html
slides                  pdf
handouts etc.           pdf
textbooks               pdf, epub
documentation           html
==================== ===============

These intended output formats do not really help chose the markup language for
the source: some compilers (the programs producing the final document) are able to
output more than one format, and there is often more than one compiler for a
given markup language.

Formatting properties
---------------------

To make a choice, listing the various formatting possibilities you need and
constraints you have for a given output is more relevant: do you need to
automatically produce a table of contents? Footnotes or sidenotes? Do you want
to insert other media (sound, video) or links to those? This is where markup
languages will show their strengths and limits, and this is what will help you
choose the one you need for a given task.

The formatting properties needed are often divided in two classes: the *inline*
formatting, applying a given format to a single word, and the *block* creation
and formatting for everything else.

The inline formatting, bold, italic, etc. is standard in all markup languages.
All of them also allow to insert inline links, be it internal or external (i.e.,
URLs) links. This does not really help to chose one language, except if you want
to be nitty-picky about your favourite syntax, or need things like having bold
italic letters inside a bold underlined word. By the way, if you think you need
this, sorry but you are probably wrong :)

Footnotes (or sidenotes), citations and index entries are very special inline
formatting: the idea is not necessary to change the apparence of the marked
word, but to create a block referring to it somewhere else in the document.

A far more interesting topic is arbitrary blocks creation and formatting. Any
piece of text is made of a hierarchy of blocks: a section is a block which might
contain a paragraph, followed by an example which itself contains 2 paragraphs,
and then a list. The example and the list are special kinds of paragraphs, and
you might imagine many others: a theorem, a warning, an equation, a citation, a
listing of code etc.

Sadly enough, most of the markup languages listed above, LaTeX included, mark
chapters, sections etc. by their title only: a section starts with a section
title, and ends with the start of the next section or higher level block. There
is no special mark to end a section, which means that you cannot get something
like the following without "cheating":

- section 1 start

  - a paragraph
  - subsection 1 start

    - a paragraph
    - an example
    - a paragraph

  - subsection 2 start

    - a paragraph

  - a paragraph which belongs to section 1, but not to subsection 2

- section 2 start
  ...

You see the problem: without a way to mark the end of subsection 2, there is no
way to tell that the last paragraph of section 1 does not belong to it. And this
is not a convoluted example refering to a proof-of-concept,
never-seen-in-real-life situation: it is rather common for example to finish a
section with a general conclusion, which does not belong to its last subsection.

The need to be able to create (and style) arbitrary blocks (sections, examples,
admonitions...) is really important to me. You might also note that, although
there is no immediate way to mark those blocks in mot of the source markup
languages, it is trivial in the destination: html, epub or pdf allow for any
kind of fancy styling, framed sections, examples etc.

TODO separate pb of sections from arbitrary blocks, explain that all these are
simply divs or blocks with properties such as a title etc.

Markdown or not markdown?
-------------------------




 



