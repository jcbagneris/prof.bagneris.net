Calculate efficiently
=====================

:lang: en
:authors: JcB
:status: published
:date: 2017-05-08 10:49
:tags: calculator, spreadsheet, rounding, finance
:summary: Some students are able to reason properly, and
          apply the right formula, then fail on the last (and simplest) step:
          calculating the result. So I wrote a short
          handout explaining how to calculate properly with an electronic
          calculator or a spreadsheet.

I am always surprised to discover that many students routinely use tools
that they don't master -- or even don't actually know how to use
properly. This is particularly striking when it comes to calculation.

My *hands-on* way of teaching and the number of numerical exercises we do in
class showed me that a very common cause of errors and wrong results is in the
last step of solving a problem: doing the calculation. Yes, some students are
able to reason properly, apply the right formula correctly, replace variables
with the provided data without any problem, **and then fail on simply
calculating the result**. They don't know how to use the memory registers of
their calculator, they vaguely remember something about parentheses and the
priority of operations but cannot explain it clearly, and when a result is
clearly wrong (given the context), they don't even see it. And it is even worse
if we use a spreadsheet. I could tell you horror stories of students calculating
something on their calculator, and then inputting the (truncated) result in the
spreadsheet (with a typo on the go), hardcoding values everywhere and generally
managing to produce such a messy table that even they cannot understand what
they meant the day after.

It happens that recently, at #mainjob, I was asked to produce contents to
prepare MBA students for various finance courses. MBA students typically have
very different backgrounds, and many did not study business, management or
economy before, but medicine, law or engineering, for example. Some never
studied some necessary topics such as time value of money, some need a
refresher. I took this opportunity to produce a (not so) short handout about
calculating properly with the basic tools we have: electronic calculators and
spreadsheets. It starts by making sure we have correct basis about how to write
properly a given mathematical expression, and then goes to calculating the
result, checking that it sounds reasonable, rounding it, re-using it in a later
calculation. It is packed with examples and I hope it proves useful in my future
classes.

`The latest pdf version
<https://files.bagneris.net/finance/std/tools/calculations/en/latest.html>`__
is freely available, and it is
`quite liberally licensed <http://creativecommons.org/licenses/by-nc-sa/4.0/>`__
(just credit me as author), so feel free to use it in your own classes. I
always appreciate a word of comment, suggestions and corrections. If you
are a bit tech-savvy, you might even fork the source on
`github <https://github.com/jcbagneris/finance-sources>`__ or send me a patch.
