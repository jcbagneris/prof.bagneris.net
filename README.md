# prof.bagneris.net

Sources of prof.bagneris.net
The site is rendered by [pelican](https://github.com/getpelican/pelican)

This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
License <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
The terms of this license allow you to remix, tweak, and
build upon this work non-commercially, as long as you credit me
and license your new creations under the identical terms.
